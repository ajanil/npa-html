$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        responsiveClass:true,
        autoplay:true,
        autoplayTimeout:2000,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })
  });

//   calendar load
document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
    
      plugins: [ 'dayGrid' ],
      events: [
        {
          title  : 'event1',
          start  : '2019-04-01'
        },
        {
          title  : 'event2',
          start  : '2019-04-05',
          end    : '2019-04-07',
        //   rendering: 'background'
        },
        {
          title  : 'event3',
          start  : '2019-04-09T12:30:00',
          allDay : false // will make the time show
        }
      ]
    });

    calendar.render();
  });